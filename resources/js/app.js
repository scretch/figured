
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


/**
 * Froala section
 */

// Require Froala Editor js file.
require('froala-editor/js/froala_editor.pkgd.min')

// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('froala-editor/css/froala_style.min.css')

// Import and use Vue Froala lib.
import VueFroala from 'vue-froala-wysiwyg'
Vue.use(VueFroala)


/**
 * bootstrap-vue section
 */
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';


Vue.use(BootstrapVue);



import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

window.Vue = require('vue');



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('admin-blogpost-index-component', require('./components/AdminBlogPostIndexComponent.vue'));
Vue.component('admin-blogpost-component', require('./components/AdminBlogPostComponent.vue'));
Vue.component('blogpost-component', require('./components/BlogPostComponent.vue'));
Vue.component('blogpost-modal-component', require('./components/CreateBlogPostModalComponent.vue'));
Vue.component('vue-pagination', require('./components/Pagination.vue'));


const app = new Vue({
    el: '#app'
});
