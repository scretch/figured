<?php
return [
    'create_success'      => 'Blog Post Successfully Created',
    'update_success'      => 'Blog Post Successfully Updated',
    'delete_success'      => 'Blog Post Successfully Deleted',

];