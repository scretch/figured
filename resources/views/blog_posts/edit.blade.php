@extends('layouts.admin')
@section('content')
    <h2>Edit Blog Post</h2><br/>
    @php
        $form_fields = [
            [
                'name'      => 'title',
                'type'      => 'text',
                'label'     => 'Title',
                'value'     => $blogpost->title
            ],
            [
                'name'      => 'intro',
                'type'      => 'text',
                'label'     => 'Introduction',
                'value'     => $blogpost->intro
            ],
            [
                'name'      => 'content',
                'type'      => 'textarea',
                'label'     => 'Content',
                'value'     => $blogpost->content
            ],

            [
                'name'      => 'published',
                'type'      => 'boolean',
                'label'     => 'Published',
                'value'     => $blogpost->published
            ]

        ];
    @endphp
    <form method="post" action="{{route('admin.blogposts.update',['id'=>$blogpost->id])}}">
        @include('includes.form')
    </form>
@endsection