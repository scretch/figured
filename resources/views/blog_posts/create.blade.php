@extends('layouts.admin')
    @section('content')
    <h2>Create Blog Post</h2><br/>
    @php
        $form_fields = [
            [
                'name'      => 'title',
                'type'      => 'text',
                'label'     => 'Title',
                'value'     => ''
            ],
            [
                'name'      => 'intro',
                'type'      => 'text',
                'label'     => 'Introduction',
                'value'     => ''
            ],
            [
                'name'      => 'content',
                'type'      => 'textarea',
                'label'     => 'Content',
                'value'     => ''
            ],

            [
                'name'      => 'published',
                'type'      => 'boolean',
                'label'     => 'Published',
                'value'     => false
            ]

        ];
    @endphp
    <form method="post" action="{{route('admin.blogposts.create')}}">
        @include('includes.form')
    </form>
    @endsection