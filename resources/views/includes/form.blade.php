        @section('scripts')
            @parent

            <script src="https://cloud.tinymce.com/5/tinymce.min.js"></script>

        @endsection

        @csrf
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @foreach($form_fields as $field)
            @switch($field['type'])
                @case('date')
                    {!! form_date_field($field['name'],$field['label'],$field['value']) !!}
                @break
                @case('boolean')
                    {!! form_boolean_field($field['name'],$field['label'],$field['value']) !!}
                @break
                @case('textarea')
                    {!! form_textarea_field($field['name'],$field['label'],$field['value']) !!}
                @break


                @default
                {!! form_text_field($field['name'],$field['label'],$field['value']) !!}
            @endswitch
        @endforeach



        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <button type="submit" class="btn btn-success">Save</button>
            </div>
        </div>

