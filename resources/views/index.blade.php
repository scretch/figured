@extends('layouts.public')
@section('header')
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Figured</h1>
            <span class="subheading">Example Blog</span>
          </div>
        </div>
      </div>
    </div>
  </header>
@endsection
@section('content')
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        @foreach($blogposts as $post)
        <div class="post-preview">
          <a href="{{route('post',['id'=>$post->id])}}">
            <h2 class="post-title">
              {{$post->title}}
            </h2>
            <h3 class="post-subtitle">
              {{$post->intro}}
            </h3>
          </a>
          <p class="post-meta">Posted
            <a href="#">@if($user=App\User::find($post->user_id)) by {{$user->name}} @endif</a>
            on {{$post->updated_at->toCookieString()}}</p>
        </div>
        <hr>
        @endforeach

        <!-- Pager -->
        <div class="clearfix text-center">
          {!! $blogposts->links() !!}
        </div>
      </div>
    </div>
  </div>
@endsection
