<?php


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        #TODO: Drop and recreate collection
        if(Schema::connection('mongodb')->hasCollection('blog_posts')) return;
        Schema::connection('mongodb')->create('blog_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('intro');
            $table->text('content');
            $table->date('published');
            $table->unsignedInteger('user_id');

            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mongodb')->dropIfExists('blog_posts');
    }
}
