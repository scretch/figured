<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');


Route::get('/posts/{id}', 'HomeController@showPost')->name('post');
Route::get('/logout','Auth\LoginController@logout')->name('logout');


Route::prefix('admin')->name('admin.')->middleware(['auth'])->group(function () {

    Route::get('/', 'BlogPostController@index')->name('index');
    Route::post('/upload_image', 'BlogPostApiController@uploadImage')->name('uploadimage');

});

Route::apiResource('blogposts','BlogPostApiController');