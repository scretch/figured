# Figured Test Project


This project was developed as a skill test for Figured, it uses

  - MongoDB
  - MySQL
  - PHP7
  - Laravel
  - Vue.JS

# Features!

  - WYSIWYG Editor
  



### Todos

 - Write MORE Tests
 - Make SPA for visitors
 - Add Continuous Integration & Delivery Code

License
----

MIT
