<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class BlogPost extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'blog_posts';


    protected $fillable = [
        'title', 'intro','content','published','user_id'
    ];


    public static $store_validation_rules = [
        'title'         => 'required',
        'intro'         => 'required',
        'content'       => 'required',


    ];

    public static $update_validation_rules = [
        'title'         => 'required',
        'intro'         => 'required',
        'content'       => 'required',


    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopePublished($query)
    {
        return $query->where('published',true);
    }


}
