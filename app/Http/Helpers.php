<?php
/**
 * Created by PhpStorm.
 * User: Rudi
 * Date: 3/13/2019
 * Time: 6:24 AM
 */

function form_text_field($name,$label,$value="")
{
    return <<<FIELD
<div class="row">
    <div class="col-md-8 col-lg-8"></div>
    <div class="form-group col-md-8">
        <label for="{$name}">{$label}:</label>
        <input type="text" class="form-control" name="{$name}" value="{$value}">
    </div>
</div>
FIELD;
}

function form_textarea_field($name,$label,$value="")
{
    return <<<FIELD
<div class="row">
    <div class="col-md-8"></div>
    <div class="form-group col-md-8">
        <label for="{$name}">{$label}:</label>
        <textarea class="form-control" name="{$name}">{$value}</textarea>        
    </div>
</div>
 
  <script>tinymce.init( {
  selector: 'textarea',
  height: 500,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table paste imagetools wordcount"
  ],
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tiny.cloud/css/codepen.min.css'
  ]
} );</script>

FIELD;
}

function form_date_field($name,$label,$value="")
{
    return <<<FIELD
<div class="row">
    <div class="col-md-8"></div>
    <div class="form-group col-md-8">
        <label for="{$name}">{$label}:</label>
        <input type="date" class="form-control" name="{$name}" value="{$value}">
    </div>
</div>
FIELD;
}

function form_boolean_field($name,$label,$checked=false)
{
    $is_checked = ($checked)?"checked":"";
    return <<<FIELD
<div class="row">
    <div class="col-md-8"></div>
    <div class="form-group col-md-8">
        <label for="{$name}">{$label}:</label>
        <input type="checkbox" class="form-control" name="{$name}" value="true" {$is_checked}>
    </div>
</div>
FIELD;
}