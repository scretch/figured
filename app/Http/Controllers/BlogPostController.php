<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogPostStoreRequest;
use App\Http\Requests\BlogPostUpdateRequest;
use App\Models\BlogPost;
use Illuminate\Http\Request;

class BlogPostController extends Controller
{

    public function index()
    {
        $blogposts = BlogPost::paginate(10);
        return view('blog_posts.index',compact('blogposts'));
    }

    public function create(Request $request)
    {
        return view('blog_posts.create');
    }

    public function store(Request $r, BlogPostStoreRequest $request)
    {
        $data = $request->validated();
        $data['published']  = ($r->published=='true')?true:false;
        $data['user_id']  = auth()->user()->id;
        BlogPost::create($data);
        return redirect()->route('admin.blogposts.index')->with('success',__('blog_posts.create_success'));

    }

    public function edit($id)
    {
        $blogpost = BlogPost::findOrFail($id);
        return view('blog_posts.edit',compact('blogpost'));
    }

    public function update(Request $r,BlogPostUpdateRequest $request,$id)
    {
        $blogpost = BlogPost::findOrFail($id);
        $data = $request->validated();
        $data['published']  = ($r->published=='true')?true:false;
        $data['user_id']  = auth()->user()->id;
        $blogpost->update($data);
        return redirect()->route('admin.blogposts.index')->with('success',__('blog_posts.update_success'));
    }

    public function destroy($id)
    {
        $blogpost = BlogPost::findOrFail($id);
        $blogpost->delete();
        return redirect()->route('admin.blogposts.index')->with('success',__('blog_posts.delete_success'));

    }


}
