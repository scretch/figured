<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blogposts = BlogPost::published()->paginate(3);
        return view('index',compact('blogposts'));
    }

    public function showPost($id)
    {
        $post = BlogPost::findOrFail($id);
        return view('post',compact('post'));
    }


}
