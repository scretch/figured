<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogPostStoreRequest;
use App\Models\BlogPost;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class BlogPostApiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogposts = BlogPost::paginate(5);
        return $blogposts;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r, BlogPostStoreRequest $request)
    {
        $data = $request->validated();
        $data['published']  = ($r->published=='true')?true:false;
        $data['user_id']  = auth()->user()->id;
        $blogpost = BlogPost::create($data);
        return $blogpost;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blogpost = BlogPost::findOrFail($id);
        return $blogpost;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, BlogPostStoreRequest $request, $id)
    {
        $blogpost = BlogPost::findOrFail($id);

        $data = $request->validated();
        $data['published']  = ($r->published=='true')?true:false;
        $data['user_id']  = auth()->user()->id;
        $blogpost->fill($data);
        $blogpost->save();
        return $blogpost;
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogpost = BlogPost::findOrFail($id);
        $blogpost->delete();
        return ['status'=>true];
    }
}
